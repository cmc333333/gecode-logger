#include <gecode/int.hh>
#include <gecode/search.hh>
#include <gecode/gist.hh>

using namespace Gecode;

class FindIt : public Space {
  protected:
    IntArgs block;
    IntVarArray vars;
    IntVar oValue;
  public:
    FindIt(void) : vars(*this, 2, 0, 15), oValue(*this, 0, 50)  {
      block = IntArgs::create(16, 16, 1);
      IntVar oStart(vars[0]);
      IntVar iStart(vars[1]);

      element(*this, block, oStart, oValue);

      rel(*this, oStart, IRT_LE, iStart);
      rel(*this, oValue, IRT_GR, 20);

      branch(*this, vars, INT_VAR_SIZE_MIN, INT_VAL_MIN);
    }
    FindIt(bool share, FindIt& s) : Space(share, s) {
      vars.update(*this, share, s.vars);
      oValue.update(*this, share, s.oValue);
    }
    virtual Space* copy(bool share) {
      return new FindIt(share, *this);
    }
    void print(std::ostream& os) const {
      os << vars << " oValue: " << oValue << std::endl;
    }
};

int main(int argc, char* argv[]) {
  FindIt* m = new FindIt();
  Gist::Print<FindIt> p("Print solution");
  Gist::Options o;
  o.inspect.click(&p);
  Gist::dfs(m,o);
  delete m;

  return 0;
}
