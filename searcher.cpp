#include <gecode/int.hh>
#include <gecode/search.hh>
#include <gecode/gist.hh>
#include <stdio.h>

using namespace Gecode;

class Searcher : public Space {
  protected:
    IntArgs block;
    IntVarArray vars;
    IntVar oValue;
  public:
    Searcher(void) : vars(*this, 2, 0, 15), oValue(*this, 0, 50)  {
      block = IntArgs::create(16, 16, 1);
      IntVar oStart(vars[0]);
      IntVar iStart(vars[1]);

      element(*this, block, oStart, oValue);

      rel(*this, oStart, IRT_LE, iStart);
      rel(*this, oValue, IRT_GR, 20);

      branch(*this, vars, INT_VAR_SIZE_MIN, INT_VAL_MIN);
    }
    Searcher(bool share, Searcher& s) : Space(share, s) {
      vars.update(*this, share, s.vars);
      oValue.update(*this, share, s.oValue);
    }
    virtual Space* copy(bool share) {
      return new Searcher(share, *this);
    }
    void print(std::ostream& os) const {
      os << vars << " oValue: " << oValue << std::endl;
    }

    Space* dfs(Space* s) {
      print(std::cout);
      switch (s->status()) {
        case SS_FAILED:
          printf("fail\n");
          break;
        case SS_SOLVED:
          printf("done\n");
          (void) s->choice(); return s;
          break;
        case SS_BRANCH:
          const Choice* ch = s->choice();
          Space* c = s->clone();
          printf("Taking first branch\n");
          s->commit(*ch,0);
          if (Space* t = dfs(s)) {
            delete ch;
            delete c;
            printf("Second branch ignored\n");
            return t;
          }
          printf("Taking second branch\n");
          s->commit(*ch,1);
          delete ch;
          return dfs(c);
      }
    }
};

int main(int argc, char* argv[]) {
  Searcher* m = new Searcher();
  //Gist::Print<Searcher> p("Print solution");
  //Gist::Options o;
  //o.inspect.click(&p);
  //Gist::dfs(m,o);
  m->dfs(m);
  delete m;

  return 0;
}
