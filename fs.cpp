#include <gecode/int.hh>
#include <gecode/search.hh>
#include <gecode/gist.hh>
#include <gecode/minimodel.hh>

using namespace Gecode;

//  What we are searching for
const IntArgs searchText(6, 's', 'e', 'c', 'r', 'e', 't');
//  Magic numbers for JPG
const int jpgStart = 65496;
const int jpgEnd = 65497;
//const IntArgs hd = IntArgs::create(16, 16, 1);
const IntArgs file(12, jpgStart, 11, 2, 4, jpgStart, 's', 'e', 'c', 'r', 'e', 't', jpgEnd, 1);
IntArgs hd(10, 4, jpgEnd, 10, 8, jpgStart, 9, 11, 14, 27, jpgEnd);

class FS : public Space {
  private:
    void fileAddiesConstraint() {
      element(*this, hd, filePtrIdx, fileStartIdx);

      rel(*this, filePtrIdx < fileStartIdx);
    }
    void fileConstraint() {
      rel(*this, fileStartIdx <= fileEndIdx);

      element(*this, hd, fileStartIdx, jpgStart);
      element(*this, hd, fileEndIdx, jpgEnd);
    }
    void textConstraint() {
      rel(*this, textMatchIds, IRT_LE);

      for (int i = 0; i < searchText.size(); i++) {
        element(*this, hd, textMatchIds[i], searchText[i]);
      }

      rel(*this, textMatchIds[0] >= fileStartIdx);
      rel(*this, textMatchIds[searchText.size() - 1] <= fileEndIdx);
    }
  protected:
    IntVarArray textMatchIds;
    IntVar fileStartIdx;
    IntVar fileStartIdxPlusOne;
    IntVar fileEndIdx;
    IntVar filePtrIdx;
  public:
    FS(void) {//: vars(*this, 2, 0, 15), oValue(*this, 0, 50)  {
      hd << file;
      hd << IntArgs(5, 2, jpgStart, 9, 10, 3);  //  filler
      hd << file;
      textMatchIds = IntVarArray(*this, searchText.size(), 0, hd.size() - 1);
      fileStartIdx = IntVar(*this, 0, hd.size() - 3);
      fileStartIdxPlusOne = IntVar(*this, 0, hd.size() - 2);
      fileEndIdx = IntVar(*this, 0, hd.size() - 1);
      filePtrIdx = IntVar(*this, 0, hd.size() - 1);

      rel(*this, fileStartIdxPlusOne == fileStartIdx + 1);

      fileAddiesConstraint();
      fileConstraint();
      textConstraint();

      branch(*this, fileStartIdx, INT_VAL_MIN);
      //branch(*this, textMatchIds, INT_VAR_SIZE_MIN, INT_VAL_MIN);
    }
    FS(bool share, FS& s) : Space(share, s) {
      textMatchIds.update(*this, share, s.textMatchIds);
      fileStartIdx.update(*this, share, s.fileStartIdx);
      fileStartIdxPlusOne.update(*this, share, s.fileStartIdxPlusOne);
      fileEndIdx.update(*this, share, s.fileEndIdx);
      filePtrIdx.update(*this, share, s.filePtrIdx);
    }
    virtual Space* copy(bool share) {
      return new FS(share, *this);
    }
    void print(std::ostream& os) const {
      os << filePtrIdx << std::endl;
      os << "tM: " << textMatchIds << " fS: " << fileStartIdx << " fE: " << fileEndIdx;
    }
};

int main(int argc, char* argv[]) {
  FS* m = new FS();
  Gist::Print<FS> p("Print solution");
  Gist::Options o;
  o.inspect.click(&p);
  Gist::dfs(m,o);
  delete m;

  return 0;
}

