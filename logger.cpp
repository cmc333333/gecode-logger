#include "logger.hh"

class PropWrapper : public Propagator
{
  Propagator* prop;
  public:
    PropWrapper (Space& home, Propagator* prop): Propagator(home)
    {
      this->prop = prop;
    }
    virtual Propagator* copy (Space& home, bool share)
    {
      Propagator* prop_copy = (Propagator*)(prop->copy(home, share));
      return new (home) PropWrapper(home, prop_copy);
    }
    virtual size_t dispose (Space& home)
    {
      prop->dispose(home);
      (void) Propagator::dispose(home);
      return sizeof (*this);
    }
    virtual PropCost cost(const Space& home, const ModEventDelta& med) const
    {
      return prop->cost(home, med);
    }
    virtual ExecStatus propagate (Space& home, 
        const Gecode::ModEventDelta& med) {
      ExecStatus stat = prop->propagate(home, med);
      std::cout << stat << std::endl;
      return stat;
    }
};

void Path::Edge::init(LoggableSpace* s, LoggableSpace* c0) {
  //arch = new Archive();
  ch = s->choice();
  //ch->archive(*arch);
  //s->print(std::cout);
  //std::cout << ch->alternatives() << " branches" << std::endl;
  a = 0;
  c = c0;
}

void Path::Edge::commit(LoggableSpace* s) {
  //std::cout << "Before: ";
  //s->print(std::cout);
  s->commit(*ch,a);
  //std::cout << "After: ";
  //s->print(std::cout);
}

LoggableSpace* Path::Edge::lao(void) {
  if (!la() || (c == NULL))
    return NULL;
  LoggableSpace* t = c;
  c = NULL;
  commit(t);
  return t;
}

void Path::Edge::reset(void) {
  delete ch;
  ch = NULL;
  delete c;
  c = NULL;
}

void Path::push(LoggableSpace* s, LoggableSpace* c) {
  if (n == n_stack)
    throw StackOverflow("Path::push");
  e[n].init(s,c);
  e[n].commit(s);
  n++;
}
bool Path::next(void) {
  while (n > 0) {
    if (e[n-1].la()) {
      e[--n].reset();
    } else {
      e[n-1].next();
      return true;
    }
  }
  return false;
}
LoggableSpace* Path::recompute(unsigned int& d) {
  if (LoggableSpace* t = e[n-1].lao()) {
    e[--n].reset();
    d = c_d;
    return t;
  }
  unsigned int i = n-1;
  for (; e[i].clone() == NULL; i--) {}
  LoggableSpace* s = e[i].clone()->clone();
  d = n - 1;
  if (d >= a_d) {
    unsigned int m = (i + d)/2;
    for (; i < m; i++)
      e[i].commit(s);
    for (; (i < n) && e[i].la(); i++)
      e[i].commit(s);
    if (i < n-1) {
      if (s->status() == SS_FAILED) {
        delete s;
        for (; i < n; n--) {
          e[n-1].reset();
          d--;
        }
        return NULL;
      }
      e[i].clone(s);
      d = n-i;
    }
  }
  for (; i< n; i++)
    e[i].commit(s);
  return s;
}

int LoggableSpace::register_prop(const std::string name) {
  prop_names.push_back(name);
  return (int)(prop_names.size());
}
void LoggableSpace::register_failed(int prop_idx) {
  failed_prop_idx = prop_idx;
}
std::string LoggableSpace::failed_reason() {
  if (failed_prop_idx < 0 || prop_names.empty())
    return std::string("Unknown Propagator");
  else {
    std::list<std::string>::iterator itty = prop_names.begin();
    if (failed_prop_idx > 0)
      std::advance(itty, failed_prop_idx-1);

    std::ostringstream to_return;
    to_return << (*itty) << " (" << failed_prop_idx << ") " ;
    failed_prop_idx = -1; //  reset
    return to_return.str();
  }
}

LoggableSpace* LoggerEngine::next(void) {
  do {
    while (s != NULL) {
      StatusStatistics ss;
      switch (s->status(ss)) {
        case SS_FAILED:
          s->print(std::cout);
          std::cout << "FAIL: " << s->failed_reason() << " failed" << std::endl;
          std::cout << std::endl;
          delete s;
          s = NULL;
          break;
        case SS_SOLVED:
          {
            s->print(std::cout);
            std::cout << "SUCCESS" << std::endl << std::endl;
            LoggableSpace* t = s;
            s = NULL;
            (void) t->choice();
            return t;
          }
        case SS_BRANCH:
          if (d >= c_d) {
            p.push(s, s->clone());
            d = 1;
          } else {
            p.push(s, NULL);
            d++;
          }
      }
    }
    while ((s == NULL) && p.next())
      s = p.recompute(d);
  } while (s != NULL);
  return NULL;
};
