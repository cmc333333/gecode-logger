#ifndef LOGGER_HH
#define LOGGER_HH

#include <gecode/int.hh>
#include <list>

using namespace Gecode;

const unsigned int c_d = 5;
const unsigned int a_d = 2;

const unsigned int n_stack = 1024;


class LoggableSpace : public Space {
  protected:
    int failed_prop_idx;
    std::list<std::string> prop_names;
  public:
    LoggableSpace(bool share, LoggableSpace& s) : Space(share, s) {
      prop_names = s.prop_names;
      failed_prop_idx = -1;
    }
    LoggableSpace() : Space() {
      prop_names = std::list<std::string>();
      failed_prop_idx = -1;
    }
    int register_prop(const std::string name);
    void register_failed(int prop_idx);
    std::string failed_reason();
    virtual void print(std::ostream& os) = 0;
    virtual LoggableSpace* clone() {
      LoggableSpace* log_clone = (LoggableSpace*)(Space::clone());
      log_clone->failed_prop_idx = failed_prop_idx;
      return log_clone;
    }
};

class StackOverflow : public Exception {
  public:
    StackOverflow(const char* l) : Exception(l, "Stack overflow") {}
};

class Path {
  protected:
    class Edge {
      protected:
        unsigned int a;
        LoggableSpace* c;
        Archive* arch;
      public:
        const Choice* ch;
        void init(LoggableSpace* s, LoggableSpace* c0);
        LoggableSpace* clone(void) const {
          return c;
        }
        void clone(LoggableSpace* s) {
          c = s->clone();
        }
        void next(void) {
          a++;
        }
        bool la(void) const {
          return a+1 == ch->alternatives();
        }
        void commit(LoggableSpace* s);
        LoggableSpace* lao(void);
        void reset(void);
    };
    unsigned int n;
  public:
    Edge e[n_stack];
    Path(void) : n(0) {}
    void push(LoggableSpace* s, LoggableSpace* c);
    bool next(void);
    LoggableSpace* recompute(unsigned int& d);
};

class LoggerEngine {
  protected:
    Path p;
    LoggableSpace* s;
    unsigned int d;
  public:
    LoggerEngine(LoggableSpace* r) : s(r), d(c_d) {
      std::cout << "FULL SPACE:" << std::endl;
      r->print(std::cout);
      std::cout << std::endl << std::endl;
    }
    LoggableSpace* next(void);
    ~LoggerEngine(void) {
      delete s;
    }
};

#endif
